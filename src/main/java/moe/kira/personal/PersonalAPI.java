package moe.kira.personal;

import java.io.File;
import java.io.IOException;
import java.util.Map;

import javax.annotation.concurrent.NotThreadSafe;

import org.bukkit.Bukkit;
import org.bukkit.command.CommandSender;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.EventPriority;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerLoginEvent;
import org.bukkit.event.player.PlayerQuitEvent;
import org.bukkit.scheduler.BukkitTask;

import com.google.common.collect.Maps;

import lombok.Getter;
import lombok.SneakyThrows;
import moe.kira.personal.misc.MemoryAccessor;
import moe.kira.personal.plugin.PersonalEngine;

@NotThreadSafe
public class PersonalAPI implements Listener {
    @Getter
    private final static MemoryAccessor<YamlConfiguration> accessorPlayer = new MemoryAccessor<YamlConfiguration>();
    @Getter
    private final static MemoryAccessor<YamlConfiguration> accessorCustom = new MemoryAccessor<YamlConfiguration>();
    
    private static YamlConfiguration loadToMemory(String player) {
        return accessorPlayer.put(player, loadConfiguration(PersonalEngine.getInstance().getDataFolder(), player));
    }
    
    private static YamlConfiguration loadToMemory(String folder, String customName, String customField) {
        return accessorCustom.put(customField, loadConfiguration(new File(PersonalEngine.getInstance().getDataFolder(), folder), customName.concat(".yml")));
    }
    
    public static YamlConfiguration of(CommandSender player) {
        return of(player.getName());
    }
    
    public static YamlConfiguration of(Player player) {
        return of(player.getName());
    }
    
    public static YamlConfiguration of(String player) {
        YamlConfiguration conf = accessorPlayer.get(player);
        if (conf == null)
            conf = loadToMemory(player);
        return conf;
    }
    
    public static YamlConfiguration of(String folder, String customeName) {
        String customField = folder.concat(":".concat(customeName));
        YamlConfiguration conf = accessorCustom.get(customField);
        if (conf == null)
            conf = loadToMemory(folder, customField, customField);
        return conf;
    }
    
    @SneakyThrows
    private static YamlConfiguration loadConfiguration(File folder, String username) {
        File file = new File(folder, username.concat(".yml"));
        
        if (!folder.exists())
            folder.mkdir();
        else
            if (file.isDirectory())
                file.delete();
        
        if (!file.exists())
            file.createNewFile();
        
        return YamlConfiguration.loadConfiguration(file);
    }
    
    private final Map<String, BukkitTask> delayedGCTasks = Maps.newHashMap();
    
    @EventHandler(priority = EventPriority.LOWEST)
    public void onPlayerLogin(PlayerLoginEvent event) {
        BukkitTask delayedTask = delayedGCTasks.get(event.getPlayer().getName());
        if (delayedTask == null) {
            loadToMemory(event.getPlayer().getName());
        } else {
            delayedTask.cancel();
        }
    }
    
    @EventHandler(priority = EventPriority.MONITOR)
    public void onPlayerQuit(PlayerQuitEvent event) {
        delayedGCTasks.put(
                event.getPlayer().getName(),
                Bukkit.getScheduler().runTaskLater(PersonalEngine.getInstance(), () -> {
                    try {
                        accessorPlayer.remove(event.getPlayer().getName()).save(new File(PersonalEngine.getInstance().getDataFolder(), event.getPlayer().getName() + ".yml"));
                    } catch (IOException ex) {
                        ex.printStackTrace();
                    }
                    delayedGCTasks.remove(event.getPlayer().getName());
                }, 30 * 20)
        );
    }
}
