package moe.kira.personal.plugin;

import java.io.File;
import java.util.Map.Entry;

import org.apache.commons.lang.StringUtils;
import org.bukkit.Bukkit;
import org.bukkit.configuration.file.YamlConfiguration;
import org.bukkit.plugin.java.JavaPlugin;

import lombok.Getter;
import lombok.SneakyThrows;
import moe.kira.personal.PersonalAPI;

public class PersonalEngine extends JavaPlugin {
    @Getter private static PersonalEngine instance;
    
    @Override
    public void onEnable() {
        instance = this;
        instance.getDataFolder().mkdir();
        Bukkit.getPluginManager().registerEvents(new PersonalAPI(), this);
    }
    
    @Override
    @SneakyThrows
    public void onDisable() {
        for (Entry<String, YamlConfiguration> entry : PersonalAPI.getAccessorPlayer().entrySet()) {
            entry.getValue().save(new File(getDataFolder(), entry.getKey() + ".yml"));
        }
        for (Entry<String, YamlConfiguration> entry : PersonalAPI.getAccessorCustom().entrySet()) {
            entry.getValue().save(new File(new File(getDataFolder(), StringUtils.substringBefore(entry.getKey(), ":")), StringUtils.substringAfter(entry.getKey(), ":") + ".yml"));
        }
    }
}
