package moe.kira.personal.misc;

import java.util.Map;
import java.util.Map.Entry;

import javax.annotation.concurrent.NotThreadSafe;

import java.util.Set;

import com.google.common.collect.Maps;

import lombok.RequiredArgsConstructor;

@NotThreadSafe
@RequiredArgsConstructor
public class MemoryAccessor<E> {
    private final Map<String, E> memory = Maps.newHashMap();
    
    public E get(String accessKey) {
        return memory.get(accessKey);
    }
    
    public E put(String accessKey, E value) {
        memory.put(accessKey, value);
        return value;
    }
    
    public E remove(String accessKey) {
        return memory.remove(accessKey);
    }
    
    public Set<Entry<String, E>> entrySet() {
        return memory.entrySet();
    }
}
